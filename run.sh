sudo apt update
sudo apt install $CI_REGISTRY -y
sudo kill $(ps aux | grep java | grep -v 'grep' | awk '{print $2}')
sudo docker system prune --all -f
sudo docker login -u "$DOCKER_HUB_LOGIN" -p "$CI_BUILD_TOKEN" $CI_REGISTRY
sudo docker run -p 8080:8080 -d "$CI_REGISTRY_IMAGE"