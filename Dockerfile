FROM openjdk:17-jdk-alpine AS builder

RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring

ARG JAR_FILE=target/main-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar

FROM openjdk:17-jdk-alpine AS app

COPY --from=builder app.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]
EXPOSE 8080